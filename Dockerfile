FROM node:10

COPY ["package.json", "/usr/app/"]

WORKDIR /usr/app

RUN npm install

COPY [".", "/usr/app/"]

EXPOSE 8000

CMD ["npm", "start"]
