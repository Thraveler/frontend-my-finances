import { Header } from "./components/Header";
import { NavBar } from "./components/NavBar";
import { Footer } from "./components/Footer";
import { LoginForm } from "./components/LoginForm";
import { SignupForm } from "./components/SignupForm";
import { MovementForm } from "./components/MovementForm";
import { Movement } from "./components/Movement";
import { Cryptocoin } from "./components/Cryptocoin";


customElements.define('header-component', Header);
customElements.define('navbar-component', NavBar);
customElements.define('footer-component', Footer);
customElements.define('login-component', LoginForm);
customElements.define('signup-component', SignupForm);
customElements.define('movement-form-component', MovementForm);
customElements.define('movement-component', Movement);
customElements.define('cryptocoin-component', Cryptocoin);