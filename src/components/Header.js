import { LitElement, html, css } from "lit-element";

export class Header extends LitElement {
  constructor() {
    super();
    this.userId = sessionStorage.getItem("userId");
  }

  static get properties() {
    return {
      userId: { type: String }
    }
  }

  static get styles() {
    return css`
      * {
        margin: 0;
        padding: 0;
      }

      header {
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: #072146;
        color: white;
        height: 60px;
      }
    `
  }

  render() {
    return html`
      <header>
        <h1>Mis Finanzas</h1>
      </header>
    `;
  }

  cerrarSesion() {
    sessionStorage.clear();
    window.location = 'login.html';
  }
}

