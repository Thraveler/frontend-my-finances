import { LitElement, html, css } from "lit-element";

export class SignupForm extends LitElement {
  constructor() {
    super();
  }

  static get properties() {
    return {
    }
  }

  static get styles() {
    return css`
      .form {
        box-sizing: border-box;
        display: flex;
        flex-direction: column;
        padding: 10px;
        color: #072146;
        height: 100%;
      }

      label {
        margin-top: 10px;
        margin-bottom: 3px;
        font-size: 1.2em;
      }

      input {
        height: 35px;
      }

      button {
        margin-top: 30px;
        height: 35px;
        background-color: #072146;
        color: white;
        font-size: 1.2em;
        border-radius: 10px;
        outline: none;
        border: none;
      }

      button:hover {
        background-color: white;
        color: #072146;
      }

      p, h2 {
        align-self: center;
      }

      a {
        text-decoration: none;
      }
    `
  }

  render() {
    return html`
        <div class="form">
          <h2>Únete a Mis finanzas</h2>
          <label for="username">Username</label>
          <input id="username" name="username" type="text" placeholder="Thraveler">
          <label for="name">Name</label>
          <input id="name" name="name" type="text" placeholder="Daniel">
          <label for="lastname">Lastname</label>
          <input id="lastname" name="lastname" type="text" placeholder="Méndez">
          <label for="email">Email</label>
          <input id="email" name="email" type="text" placeholder="mail@gmail.com">
          <label for="password">Password</label>
          <input id="password" name="password" type="password" placeholder="secretpassword">
          <button @click="${this.signup}">Login</button>
          <p>Don't have an account? <a href="./login.html">Click here!</a></p>
        </div class="form">
    `;
  }

  async signup() {
    let user = {
      username: this.shadowRoot.getElementById('username').value,
      name: this.shadowRoot.getElementById('name').value,
      lastname: this.shadowRoot.getElementById('lastname').value,
      email: this.shadowRoot.getElementById('email').value,
      password: this.shadowRoot.getElementById('password').value
    }

    let url = "http://localhost:3000/api/users";
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify(user)
    };
    let response = await fetch(url, options);

    if(response.ok) {
      alert("Usuario creado con éxito")
      window.location = 'login.html';
    } else {
      let json = await response.json();

      alert(json.error);
    }

  }
}

