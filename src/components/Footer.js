import { LitElement, html, css } from "lit-element";

export class Footer extends LitElement {
  constructor() {
    super();
  }

  static get properties() {
    return {
    }
  }

  static get styles() {
    return css`
      * {
        margin: 0px;
        padding 0px;
      }
      footer {
        background-color: #072146;
        color: white;
        height: 70px;
      }

      footer {
        text-align: center;
        padding: 10px;
      }

      p { 
        font-size: 1.2em;
        margin: 8px 0;
      }

      div {
        display: flex;
        justify-content: center;
      }

      i {
        font-size: 2em !important;
        margin: 0 15px;
        transition-duration: 0.3s;
        transition-property: transform;
      }

      i:hover {
        cursor: pointer;
        transform: scale(1.2);
      }
    `
  }

  render() {
    return html`
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

      <footer>
        <p>Hecho con amor</p>
        <div>
          <i class="fa fa-facebook-square" aria-hidden="true"></i>
          <i class="fa fa-twitter-square" aria-hidden="true"></i>
          <i class="fa fa-instagram" aria-hidden="true"></i>
          <i class="fa fa-youtube-play" aria-hidden="true"></i>
        </div>
      </footer>
    `;
  }
}

