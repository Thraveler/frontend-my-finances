import { LitElement, html, css } from "lit-element";

export class Movement extends LitElement {
  constructor() {
    super();
    this.movements = [];
    this.userId = sessionStorage.getItem("userId");
    this.getUserMovements(this.userId);
    this.totalIngresos = 0;
    this.totalGastos = 0;
    sessionStorage.removeItem("movementId");
  }

  static get properties() {
    return {
      movements: { type: Array },
      userId: { type: String },
      totalIngresos: { type: Number },
      totalGastos: { type: Number },
    }
  }

  static get styles() {
    return css`
      * {
        margin: 0;
        padding: 0;
      }

      .movement {
        margin: 10px;
        padding: 10px;
        border: 1px solid gray;
        border-radius: 5px;
        box-sizing: border-box;
        box-shadow: 1px 1px 5px gray;
        background-color: #072146;
        color: white;
      }

      .movement-data {
        display: flex;
        justify-content: space-between;
        padding: 5px 0;
      }
      
      .movement-detail {
        display: flex;
        justify-content: space-between;
        padding: 5px 0;
      }
      
      .movement-options {
        display: flex;
        justify-content: flex-end;
        padding: 5px 0;
      }

      .option-button {
        padding: 5px 8px;
        margin: 0 5px;
        color: white;
        outline: none;
        border: none;
      }

      .option-button:hover {
        background-color: white;
      }
      
      .c-primary {
        background-color: #072146;
        border-color: #ededed
      }
      
      .w-primary {
        background-color: red;
      }

      .c-primary:hover {
        color: #072146;
      }

      .w-primary:hover {
        color: red;
      }

      .ingreso {
        color: green;
      }

      .gasto {
        color: red;
      }

      .date {
        font-size: .9em;
        color: gray;
      }

      .amount {
        font-size: 1.1em;
        margin-left: 20px;
      }

      .description {
        text-align: justify;
      }

      .movements-header {
        margin: 0 10px;
        display: flex;
        align-items: center;
      }

      h2 {
        margin: 15px auto 0 0;
        color: #072146;
      }

      .totals {
        display: flex;
        justify-content: space-evenly;
        margin: 10px;
        padding; 10px;
      }

      .totals-text {
        font-size: 1.1em;
        color: #072146;
      }
      `
  }

  render() {
    return html`
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      
      <div class="movements-header">
        <h2>Resumen de movimientos</h2>
        <a href="./movement.html">
          <button class="option-button c-primary">
            <i class="fa fa-plus-square" aria-hidden="true"></i> Agregar movimiento
          </button>
        </a>
      </div>
      <div class="totals">
        <p class="totals-text">Ingresos totales: <span class="ingreso">$ ${this.totalIngresos}</span></p>
        <p class="totals-text">Gastos totales: <span class="gasto">$ ${this.totalGastos}</span></p>
      </div>
      ${this.movements.map((m, i)=> {
        return html`
        <div class="movement">
          <div class="movement-data">
            <span class="date">${new Date(m.date_operation).toISOString().slice(0, 10)}</span>
            <span class="${m.type.toLocaleLowerCase()}">${m.type}</span>
          </div>
          <div class="movement-detail">
            <span class="description">${m.description}</span>
            <span class="amount">${this.formatCurrency(m.amount)}</span>
          </div>
          <div class="movement-options">
            <button class="option-button w-primary" @click="${() => this.borrarMovimiento(i, m._id) }">Borrar</button>
            <a href="./movement.html">
              <button class="option-button c-primary" @click="${() => this.editarMovimiento(m._id) }">Editar</button>
            </a>
          </div>
        </div>`;
      })}
    `;
  }

  async getUserMovements(userId) {
    let url = `http://localhost:3000/api/users/${userId}`;

    let response = await fetch(url);

    if(response.ok) {
      let json = await response.json();

      this.movements = json.user.movements;

      this.calcularTotalIngresos();
      this.calcularTotalGastos();
    } else {
      let json = await response.json();

      alert(json.error);
    }
  }

  async borrarMovimiento(index, movementId) {
    let url = `http://localhost:3000/api/movements/${movementId}`;

    let response = await fetch(url, { method: 'DELETE'} );

    if(response.ok) {
      let json = await response.json();

      alert(json.message);

      this.movements = this.movements.filter((m, i)=> i != index);
    } else {
      let json = await response.json();

      alert(json.error);
    }
  }

  editarMovimiento(movementId) {
    sessionStorage.setItem("movementId", movementId);
  }

  calcularTotalIngresos() {
    this.movements.forEach(m => {
      if(m.type == "INGRESO") this.totalIngresos += m.amount;
    });
  }
  
  calcularTotalGastos() {
    this.movements.forEach(m => {
      if(m.type == "GASTO") this.totalGastos += m.amount;
    });
  }

  formatCurrency(amount) {
    const formatter = new Intl.NumberFormat('es-MX', {
      style: 'currency',
      currency: 'MXN',
      minimumFractionDigits: 2
    });

    return formatter.format(amount);
  }
}