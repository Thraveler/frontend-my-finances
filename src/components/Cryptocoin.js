import { LitElement, html, css } from "lit-element";

export class Cryptocoin extends LitElement {
  constructor() {
    super();
    this.userId = sessionStorage.getItem("userId");
    this.getCriptoCoins();
  }

  static get properties() {
    return {
      userId: { type: String },
      coins: { type: Array }
    }
  }

  static get styles() {
    return css`
      * {
        margin: 0;
        padding: 0;
      }

      h2 {
        color: #072146;
        margin: 10px 10px;
      }

      #cripto-container {
        display: flex;
        flex-direction: column;
        color: white;
        padding-bottom: 10px;
      }

      .cripto-currency {
        background-color: #072146;
      }

      .cripto-currency {
        margin: 3px 10px;
        padding: 8px;
        border-radius: 3px;
      }

      .symbol-coin {
        font-size: .8em;
        color: gray;
      }

      .verde {
        color: green;
      }

      .rojo {
        color: red;
      }
    `
  }

  render() {
    return html`
      <h2>Precio actual de criptomonedas</h2>
      <div id="cripto-container" ?hidden="${this.coins}">
      ${this.coins.map(c => {
        return html`
        <div class="cripto-currency">
          <h4>${c.name} <span class="symbol-coin">${c.symbol}</span></h4>
          <p>Price: USD ${c.price_usd}</p>
          <p>Market cap: USD ${c.market_cap}</p>
          <p>Volume 24h: USD ${c.volume_24}</p>
          <small>% de cambio 24h <span class="${c.percent_change_24h > 0 ? "verde" : "rojo"}">${c.percent_change_24h}</span></small>
        </div>
          `
        })
      }
    </div>
    `;
  }

  async getCriptoCoins() {
    let url = "https://api.coinlore.net/api/tickers/";
    let response = await fetch(url);

    if(response.ok) {
      let json = await response.json();

      this.coins =this.formatearCoins(json.data);

      console.log(this.coins);
    } else {
      let json = await response.json();

      alert(json.error);
    }
  }

  formatearCoins(coins) {

    const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 2
    });

    return coins.map(coin => {
      return ({
        name: coin.name,
        symbol: coin.symbol,
        percent_change_24h: coin.percent_change_24h,
        price_usd: formatter.format(coin.price_usd),
        market_cap: formatter.format(coin.market_cap_usd),
        volume_24: formatter.format(coin.volume24),
      });
    });
  }
}

