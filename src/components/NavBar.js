import { LitElement, html, css } from "lit-element";

export class NavBar extends LitElement {
  constructor() {
    super();
    this.userId = sessionStorage.getItem("userId");
  }

  static get properties() {
    return {
      userId: { type: String }
    }
  }

  static get styles() {
    return css`
      * {
        margin: 0;
        padding: 0;
      }

      header {
        display: flex;
        justify-content: space-between;
        align-items: center;
        background-color: #072146;
        color: white;
        height: 45px;
      }

      header div {
        // height: inherit;
        // display: inherit;
        // align-items: center;
      }

      a {
        margin: auto 10px;
        text-decoration: none;
        color: white;
      }
    `
  }

  render() {
    return html`
    <header>
      <div>
        <a href="/">Home</a>
        <a href="cryptocoin.html">Cryptomoneda</a>
      </div>
      <div>
        <a href="login.html" @click="${this.cerrarSesion}" ?hidden="${!this.userId}">Salir</a>
      </div>
    </header>
    `;
  }

  cerrarSesion() {
    sessionStorage.clear();
    window.location = 'login.html';
  }
}

