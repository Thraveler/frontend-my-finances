import { LitElement, html, css } from "lit-element";

export class MovementForm extends LitElement {
  constructor() {
    super();
    this.userId = sessionStorage.getItem("userId");
    this.movementId = sessionStorage.getItem("movementId");
    this.movement = {
      description: '',
      amoun: '',
      date_operation: Date.now()
    }

    if(this.movementId) this.getMovement(this.movementId);

    console.log(this.movement);
  }

  static get properties() {
    return {
      userId: { type: String },
      movementId: { type: String },
      movement: { type: Object }
    }
  }

  static get styles() {
    return css`
      * {
        margin: 0;
        padding: 0;
      }

      .form {
        box-sizing: border-box;
        display: flex;
        flex-direction: column;
        padding: 10px;
        color: #072146;
        height: 100%;
      }

      label {
        margin-top: 10px;
        margin-bottom: 3px;
        font-size: 1.2em;
      }

      input, select {
        height: 35px;
        width: 100%;
        display: inline-block;
        box-sizing: border-box;
      }

      .movement-options {
        margin-top: 15px;
        display: flex;
        justify-content: flex-end;
        padding: 5px 0;
      }

      .option-button {
        padding: 5px 8px;
        margin: 0 5px;
        color: white;
        outline: none;
        border: none;
      }

      .option-button:hover {
        background-color: white;
      }
      
      .c-primary {
        background-color: #072146;
      }
      
      .w-primary {
        background-color: red;
      }

      .c-primary:hover {
        color: #072146;
      }

      .w-primary:hover {
        color: red;
      }

      p, h2 {
        align-self: center;
      }
    `
  }

  render() {
    return html`
        <div class="form">
          <h2>Detalle de movimiento</h2>
          <label for="type">Tipo</label>
          <select id="type" name="type">
            <option value="GASTO" ?selected="${(this.movement.type == "GASTO")}">Gasto</option>
            <option value="INGRESO" ?selected="${(this.movement.type == "INGRESO")}">Ingreso</option>
          </select>
          <label for="description">Descripción</label>
          <input id="description" name="description" type="text" placeholder="Pago de luz" .value="${this.movement.description}">
          <label for="amount">Cantidad</label>
          <input id="amount" name="amount" type="number" placeholder="$55.4" min=0 step="0.01" .value="${this.movement.amount}">
          <label for="date_operation">Fecha</label>
          <input id="date_operation" name="date_operation" type="date" .value="${new Date(this.movement.date_operation).toISOString().slice(0, 10)}">
          <div class="movement-options">
            <a href="./">
              <button class="option-button w-primary">Cancelar</button>
            </a>
            <button class="option-button c-primary" @click="${() => { this.saveUserMovement() } }">Guardar</button>
          </div>
        </div class="form">
    `;
  }

  saveUserMovement() {  
    console.log("hi");

    if(this.movementId) {
      this.editeUserMovement();
      console.log("Defined");
    } else {
      this.createUserMovement();
      console.log("Undefined");
    }
  }

  async createUserMovement() {
    let movement = {
      type: this.shadowRoot.getElementById('type').value,
      description: this.shadowRoot.getElementById('description').value,
      amount: this.shadowRoot.getElementById('amount').value,
      date_operation: this.shadowRoot.getElementById('date_operation').value
    }

    let url = `http://localhost:3000/api/users/movements/${this.userId}`;
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify(movement)
    };

    let response = await fetch(url, options);

    if(response.ok) {
      window.location = '';
    } else {
      let json = await response.json();
      console.log(json);
      alert(json.error);
    }
  }

  async editeUserMovement() {
    let movement = {
      type: this.shadowRoot.getElementById('type').value,
      description: this.shadowRoot.getElementById('description').value,
      amount: this.shadowRoot.getElementById('amount').value,
      date_operation: this.shadowRoot.getElementById('date_operation').value
    }

    console.log(movement);

    let url = `http://localhost:3000/api/movements/${this.movementId}`;
    let options = {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify(movement)
    };

    let response = await fetch(url, options);

    if(response.ok) {
      window.location = '';
    } else {
      let json = await response.json();
      console.log(json);
      alert(json.error);
    }
  }

  async getMovement(movementId) {
    let url = `http://localhost:3000/api/movements/${movementId}`;

    let response = await fetch(url);

    if(response.ok) {
      let json = await response.json();
      this.movement = json.movement;
    } else {
      let json = await response.json();
      console.log(json);
      alert(json.error);
    }
  }
}

